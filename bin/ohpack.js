#!/usr/bin/env node

var argv = require('minimist')(process.argv.slice(2),{ boolean:['p','w'] });

var walker = require('../lib/walker');
var config = require('../lib/config');
var wp     = require('webpack');
var path   = require('path');
var hash   = null;
var start  = argv._[0] || './';
var extra  = path.join(process.cwd(),'ohpack.config.js');

walker (start,(e,files) => {
  
  if (e) return console.log(e);
  
  config.entry = files.reduce((o,f) => { o[f.key] = f.path; return o; },{});
  config.watch = argv.w === true;
  
  if (argv.p) config.plugins.push(new wp.optimize.UglifyJsPlugin());
  
  try { config = require(extra)(config); } catch (e) {}
  
  var compiler = wp (config);
  
  function handler (error,stats)
  {
    if (!config.watch) compiler.purgeInputFileSystem();
    
    if (error) {
      hash = null;
      console.error(error.stack||error);
      if (error.details) console.error(error.details);
      if (!config.watch) process.on('exit',() => process.exit(1));
      return;
    }
    
    if (stats.hash !== hash) {
      hash = stats.hash;
      process.stdout.write(stats.toString(config) + '\n');
    }
      
    if (!config.doWatch && stats.hasErrors()) {
      process.on('exit',() => process.exit(2));
    }
  }
  
  if (argv.w) {
    compiler.watch(config,handler);
  } else {
    compiler.run(handler);
  }
  
});