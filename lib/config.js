var OhPack = require('ohpack-webpack-plugin');
var SMixer = require('stylus-mixer');
var path   = require('path');
var paths  = [
  path.resolve(__dirname,'../node_modules'),
  path.resolve(process.cwd(),'./node_modules')
];

var babels = {
  es2015: path.resolve(paths[0],'babel-preset-es2015'),
  react:  path.resolve(paths[0],'babel-preset-react' )
};

module.exports = {
  resolveLoader: { modulesDirectories:paths },
  resolve: { modulesDirectories:paths },
  stylus: { use:[SMixer()] },
  plugins: [ new OhPack() ],
  module: { loaders: [{
    test: /\.jsx$/,
    loader: 'babel-loader',
    query: { presets:[babels.es2015,babels.react] }
  },{
    test: /\.js$/,
    loader: 'babel-loader',
    query: { presets:[babels.es2015] }
  },{
    test: /\.styl(us)?$/,
    loader: OhPack.loaders.stylus
  },{
    test: /\.(jade|pug)$/,
    loader: OhPack.loaders.jade
  }]},
  output: {
    path: './',
    filename: '[name]'
  }
};
