var Walker = require('walker');
var path   = require('path');
var regex  = /(.+?)?([^/]+)\.(oh)\.(jsx?|styl(?:us)?|jade|pug)$/;
var isS    = require('lodash/isString');

function walker (start,cb)
{
  var files = [];
  var start = isS(start)
    ? start
    : './';
  
  Walker(start)
  
  .on('file',(file,stat) => {
    
    var match = file.match(regex);
    if (match) {
      var entry = {};
      entry.path   = './' + file;
      entry.dir    = match[1] ? ('./' + match[1]) : './';
      entry.prefix = match[1] || '';
      entry.name   = match[2];
      entry.oh     = match[3];
      entry.ext    = match[4];
      
      switch (entry.ext) {
        case 'js':
        case 'jsx':
          entry.type = 'javascript';
          entry.key  = entry.prefix + entry.name + '.js';
        break;
        case 'styl':
        case 'stylus':
          entry.type = 'stylus';
          entry.key  = entry.prefix + entry.name + '.css';
        break;
        case 'pug':
        case 'jade':
          entry.type = 'jade';
          entry.key  = entry.prefix + entry.name + '.html';
        break;
      }
      
      files.push(entry);
    }
    
  })
  
  .on('error',(er,en,s) => cb ({ error:er,entry:en,stat:s }))
  .on('end'  ,()        => cb (null,files))
}

module.exports = walker;